import grpc_tools.protoc

# generate service_pb2 (for proto messages) and 
# service_pb2_grpc (for RPCs) stubs
grpc_tools.protoc.main([
    'grpc_tools.protoc',
    '-I{}'.format("./protos/."),
    '--python_out=.',
    '--grpc_python_out=.',
    './protos/action.proto'
])
