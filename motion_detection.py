import numpy as np
import cv2
import logging
import datetime
import time
import camera_api;
import configuration;
from pathlib import Path
import grpc;
import action_pb2;
import action_pb2_grpc;

class Track:    
    __motionThreshold = 10    
    __font = cv2.FONT_HERSHEY_SIMPLEX
    __timeSpaceBetweenShoots = 3.0

    def __init__(self):
        self.status = False
        print("Track detection constructor")

    def __distMap(self, frame1, frame2):
        """outputs pythagorean distance between two frames"""
        frame1_32 = np.float32(frame1)
        frame2_32 = np.float32(frame2)
        diff32 = frame1_32 - frame2_32
        norm32 = np.sqrt(diff32[:,:,0]**2 + diff32[:,:,1]**2 + diff32[:,:,2]**2)/np.sqrt(255**2 + 255**2 + 255**2)
        dist = np.uint8(norm32*255)
        return dist

    def start(self):
        print("Camera started!")
        logging.info("Entering Track.start")

        self.status = True

        cap = cv2.VideoCapture(0)

        if(cap.isOpened()):
            
            _, frame1 = cap.read()
            _, frame2 = cap.read()

            lastSavedTimestamp = datetime.datetime.now()

            while(self.status):
                print("Checking Frames")
            
                _, frame3 = cap.read()
            
                dist = self.__distMap(frame1, frame3)

                frame1 = frame2
                frame2 = frame3
            
                timestamp = datetime.datetime.now()

                # apply Gaussian smoothing
                mod = cv2.GaussianBlur(dist, (9,9), 0)

                # calculate st dev test
                _, stDev = cv2.meanStdDev(mod)

                cv2.putText(frame2, "Standard Deviation - {}".format(round(stDev[0][0],0)), (70, 70), self.__font, 1, (255, 0, 255), 1, cv2.LINE_AA)

                if stDev > self.__motionThreshold:
                    if (timestamp - lastSavedTimestamp).seconds >= self.__timeSpaceBetweenShoots:
                        logging.info("Motion detected")
                        ts = timestamp.strftime("%A %d %B %Y %I_%M_%S%p")  
                        path = "{base_path}/{timestamp}.jpg".format(base_path="media", timestamp=ts)
                        safeFilePath = Path(path)
                                        
                        imageBytes = cv2.imencode('.jpg', frame2)[1]                                            

                        self.uploadImage(imageBytes, '.jpg')

                        cv2.imwrite(str(safeFilePath), frame2)
                        lastSavedTimestamp = timestamp                        
                   
                if cv2.waitKey(1) & 0xFF == 27:
                    break

                time.sleep(1)

            cap.release()
        else:
            print("Alert! Camera disconnected")        

    def stop(self):
        print("Camera stopped!")
        self.status = False    

    def yieldRequests(self, bytesArray, extension, chunk_size=1024):
        config = configuration.Config().Get()

        apiKey = config['server']['apiKey']

        metadata = action_pb2.FileUploadMetaDataRequest(apiKey=apiKey, extension=extension)
                
        yield action_pb2.FileUploadRequest(metadata=metadata)

        for i in range(0, len(bytesArray), chunk_size):                                                                    
            numpyBytesArray = bytesArray[i:i+chunk_size]
            chunk = numpyBytesArray.tobytes()
            yield action_pb2.FileUploadRequest(chunkData=chunk)                        

    def uploadImage(self, bytesArray, extension, chunk_size=1024):        
        authenticate = camera_api.Authenticate()
        bearerToken = authenticate.run()

        ssl_creds = grpc.ssl_channel_credentials(open('cert\localhost.cer', 'rb').read())
        authenticationCred = grpc.access_token_call_credentials(bearerToken)

        channelCredentials = grpc.composite_channel_credentials(
            ssl_creds,
            authenticationCred
        )                     

        url = camera_api.UriBuilder.getGrpcHostAndPortUrl()

        with grpc.secure_channel(url, channelCredentials) as channel:
            stub = action_pb2_grpc.CameraActionStub(channel)            

            requestsIterator = self.yieldRequests(bytesArray=bytesArray, extension=extension)

            stub.FileUpload(requestsIterator)
                        

    

    