import configuration
import requests

class Authenticate:
    __apiUrlPath = '/api/authenticate'           

    def __init__(self):
        self.uriBuilder = UriBuilder()

    def run(self):
        print("Entering Authenticate run")

        config = configuration.Config().Get()

        apiKey = config['server']['apiKey']

        requestModel = {'apiKey': apiKey}

        headers = {'Content-Type': 'application/json'}
        
        verifyCert = config['server']['validationCert'] == "true"

        response =  requests.post(
            self.uriBuilder.getWebsiteAbsoluteUrl(self.__apiUrlPath), 
            json = requestModel, 
            headers = headers, 
            verify=verifyCert)                

        if response.ok:
            # Retrieve JSON data
            json_data = response.json()
            
            return json_data["token"]
        else:
            # Handle error
            response.raise_for_status()
        
class UriBuilder:   
    def getWebsiteAbsoluteUrl(self, relativeUrl):
        config = configuration.Config().Get()

        result = ""

        if config['server'].get('urlSecure') == "true":
            result += "https"
        else:
            result += "http"

        result += "://" + config['server']['websiteUrl']

        if config['server'].get("urlPort", "") != "":
            result += ":" + config['server']['urlPort']

        result += relativeUrl

        return result        

    def getHostAndPortUrl(self, baseUrl):
        config = configuration.Config().Get()        

        result = baseUrl

        if config['server'].get("urlPort", "") != "":
            baseUrl += ":" + config['server']['urlPort']

        return result

    def getWebsiteHostAndPortUrl(self):
        config = configuration.Config().Get()
        
        baseUrl = config['server']['websiteUrl']
        
        return self.getHostAndPortUrl(baseUrl)
    
    def getGrpcHostAndPortUrl(self):
        config = configuration.Config().Get()
        
        baseUrl = config['server']['grpcUrl']
        
        return self.getHostAndPortUrl(baseUrl)
        
    
    



    
