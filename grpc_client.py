import os
import threading;
import grpc;
import action_pb2;
import action_pb2_grpc;
import motion_detection;
import camera_api;
import configuration;

class Camera:     
    def __init__(self) -> None:
        self.uriBuilder = camera_api.UriBuilder()
        self.config = configuration.Config().Get()
        self.certificateFolder = "cert"

    async def trackMotionDetectionStatus(self) -> None:        
       
        authenticate = camera_api.Authenticate()
        bearerToken = authenticate.run()
        
        channelCredentials = self.getCredentials(bearerToken)                        

        apiKey = self.config['server']['apiKey']

        url = self.uriBuilder.getGrpcHostAndPortUrl()                

        async with grpc.aio.secure_channel(url, channelCredentials) as channel:
            stub = action_pb2_grpc.CameraActionStub(channel)                                   

            motionDetectionStream = stub.SubscribeMotionDetection(action_pb2.MotionDetectionRequest(apiKey = apiKey))

            trackMotion = motion_detection.Track()

            while True:
                response = await motionDetectionStream.read()
                if response == grpc.aio.EOF:
                    break
                
                if response.motionDetectionEnabled:
                    x = threading.Thread(target=trackMotion.start)
                    x.start()                    
                else:
                    trackMotion.stop()

    def getCredentials(self, bearerToken):
        
        certFile = self.config['server']['certificateGrpc']

        file_path = os.path.join(self.certificateFolder, certFile)

        ssl_creds = grpc.ssl_channel_credentials(open(file_path, 'rb').read())
        authenticationCred = grpc.access_token_call_credentials(bearerToken)

        channelCredentials = grpc.composite_channel_credentials(
            ssl_creds,
            authenticationCred
        )
        
        return channelCredentials