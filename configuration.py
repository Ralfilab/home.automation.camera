import configparser
from pathlib import Path

class Config:
    __configFilePath = 'config/config.ini'
    
    def __init__(self):
        print("Config constructor")

    def Get(self):                
        config = configparser.ConfigParser()
        config.read(self.__configFilePath)
        return config