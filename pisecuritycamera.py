import logging
import actioncontroller
from os import environ

# environ["GRPC_DNS_RESOLVER"] = "native"
# environ["GRPC_VERBOSITY"] = "DEBUG"
# environ["GRPC_TRACE"] = "all"

logging.basicConfig(filename='logs/rapsberryReliability.log', level=logging.INFO)

actionController = actioncontroller.ActionController()

actionController.run()